from models import System
from components import *
import pyglet

class sPrintInfo(System):
	def __init__(self):
		super().__init__()
		self.components_needed = [type(cStringInfo(''))]

	def update(self, dt):
		# for entity, component_instance in self.entity_dict.items():
		# 	print('Entity:', entity, ' Info:', component_instance)
		pass

class sDraw(System):
	def __init__(self):
		super().__init__()
		self.components_needed = [type(cPosition(0,0)),
								  type(cTexture(None))]
		self.dt = None

	def update(self, dt):
		if dt is None:
			for entity, component_instance in self.entity_dict.items():
				print('Entity:', entity, ' Info:', self.entity_dict[entity])
		else:
			self.dt = dt