from abc import ABCMeta, abstractmethod

class Entity():
	def __init__(self, guid = None):
		self._guid = guid or id(self)

class Component():
	pass

class System(metaclass=ABCMeta):
	def __init__(self):
		self.entity_manager = None
		self.system_manager = None
		self.components_needed = []
		self.entity_dict = {}

	def build_entity_component_dict(self):
		#currently builds a entity_dict using {entity: [comp1, comp2]}
		#depending on efficiency considerations, in the future might
		#want to make a list of lists using entity.get_guid() as indexes
		self.entity_dict = {}
		for component_type in self.components_needed:
			for entity, component_instance in self.entity_manager.pairs_for_type(component_type):
				if entity in self.entity_dict.keys():
					self.entity_dict[entity].append(component_instance)
				else:
					self.entity_dict[entity] = [component_instance]

		prune_entities = []

		for entity in self.entity_dict:
			if len(self.entity_dict[entity]) < len(self.components_needed):
				self.entity_dict[entity] = None
				prune_entities.append(entity)

		for entity in prune_entities:
			del self.entity_dict[entity]

		# print(self, "\n", self.entity_dict, "\n", self.entity_dict.items())

	@abstractmethod
	def update(self, dt):
		print("{0}'s update method was called, dt = {1}".format(self.__name__, 
																dt))
