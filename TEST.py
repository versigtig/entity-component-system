from components import *

# import exceptions

# database = {}

# component_instance = component.Position(0, 0)
# component_type = type(component_instance)
# entity = 1234567

# database[component_type] = {}
# database[component_type][entity] = component_instance

# print(database)
# print(database[component_type])
# print(database[component_type][entity])

# raise exceptions.SystemAlreadyAddedToManagerError(component_instance,
# 												  component_type,
# 												  entity)

component_instance = cPosition(0,0)
component_type = type(component_instance)
print(component_type.__name__)
entity = 1001
entity_deux = 1002
entity_thrux = 1003

_database = {}
_database[component_type] = {}
_database[component_type][entity] = component_instance
_database[component_type][entity_deux] = component_instance
_database[component_type][entity_thrux] = component_instance


# print(_database)
print({}.items())
print(_database[component_type].items())
print(list(_database.keys()))

for k, v in _database[component_type].items():
	print('Key:', k, ' Value:', v)

#THIS IS HOW LAMDA WORKS DAWG
test_func_var = lambda t, y: t * 100 // y
print(test_func_var(30, 10))
