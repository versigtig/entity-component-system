import pyglet

pyglet.resource.path = ['images']
pyglet.resource.reindex()

i_player = pyglet.resource.image('player.png')
i_enemy = pyglet.resource.image('enemy.png')