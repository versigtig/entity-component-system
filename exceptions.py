'Errors my man. Errors'

class NonExistantComponentTypeForEntity(Exception):
	def __init__(self, entity, component_type):
		self.entity = entity
		self.component_type = component_type

	def __str__(self):
		return (
			    "Component type '{0}' does not exist for entity '{1}'".format(
			    	self.component_type.__name__, self.entity)
			   )

class SystemNotInSystemManagerError(Exception):
	def __init__(self, system_type, system_manager):
		self.system_type = system_type
		self.system_manager = system_manager

	def __str__(self):
		return ("System type '{0}' does not exist" 
			    "in system manager '{1}".format(self.system_type,
												self.system_manager))

class DuplicateSystemTypeError(Exception):
	def __init__(self, system_type):
		self.system_type = system_type

	def __str__(self):
		return "Duplicate system type: '{0}'".format(self.system_type.__name__)


class SystemAlreadyAddedToManagerError(Exception):
	def __init__(self, system_instance, existing_system_manager, new_system_manager):
			self.system_instance = system_instance
			self.existing_system_manager = existing_system_manager
			self.new_system_manager = new_system_manager

	def __str__(self):
		return (
			    "System '{0}' which belongs to system manger '{1}' "
				"attempted to be added to system manager '{2}'".format(
					self.system_instance, self.existing_system_manager,
					self.new_system_manager)
			   )