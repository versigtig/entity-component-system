from models import Entity
from exceptions import *

class EntityManager():
	def __init__(self):
		self._database = {}
		self._next_guid = 0

	def database(self):
		#returns the database#
		return self._database

	def create_entity(self):
		#creates an entity, does not add to the database#
		entity = Entity(self._next_guid)
		self._next_guid += 1
		return entity

	def add_component(self, entity, component_instance):
		#associates a component_instance with an entity#
		component_type = type(component_instance)
		if component_type not in self._database:
			self._database[component_type] = {}

		self._database[component_type][entity] = component_instance

	def remove_component(self, entity, component_type):
		#remove the component of component_type associated with the#
		#entity from the database#
		try:
			del self._database[component_type][entity]
			if self._database[component_type] == {}:
				del self._database[component_type]
		except KeyError:
			pass

	def pairs_for_type(self, component_type):
		try:
			return self._database[component_type].items()
		except KeyError:
			return {}.items()

	def component_for_entity(self, entity, component_type):
		#return the instance of component_type for the entity from#
		#from the database#
		try:
			return self._database[component_type][entity]
		except KeyError:
			raise NonExistantComponentTypeForEntity(entity, component_type)

	def remove_entity(self, entity):
		#removes all components from the database that are associated#
		#with the entity, also removes the entity from the database#
		for component_type in list(self._database.keys()):
			try:
				del self._database[component_type][entity]
				if self._database[component_type] == {}:
					del self._database[component_type]
			except KeyError:
				pass


class SystemManager():
	def __init__(self, entity_manager):
		self._systems = [] #a list of all systems added
		self._system_types = {} #a key/value of [system_type]: system_instance#
		self._entity_manager = entity_manager #pass this to each system on add#

	def systems(self):
		#returns the list of systems in the database#
		return self._systems

	def add_system(self, system_instance):
		system_type = type(system_instance)
		if system_type in self._system_types:
			raise DuplicateSystemTypeError(system_type)
		if system_instance.system_manager is not None:
			raise SystemAlreadyAddedToManagerError(
				  system_instance, self, system_instance.system_manager)
		system_instance.entity_manager = self._entity_manager
		system_instance.system_manager = self
		system_instance.build_entity_component_dict()
		self._system_types[system_type] = system_instance
		self._systems.append(system_instance)

	def remove_system(self, system_type):
		system = self._system_types[system_type]
		system.entity_manger = None
		system.system_manager = None
		system.entity_list = None
		self._systems.remove(system)
		del self._system_types[system_type]

	def find_system(self, system_type):
		if system_type in self._system_types:
			return self._system_types[system_type]
		else:
			raise SystemNotInSystemManagerError(system_type, self)

	def update(self, dt):
		for system in self._systems:
			system.update(dt)