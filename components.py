from models import Component

class cStringInfo(Component):
	def __init__(self, info):
		self.info = info

class cTexture(Component):
	def __init__(self, texture):
		self.texture = texture

class cPosition(Component):
	def __init__(self, x, y):
		self.x = x
		self.y = y

class cVelocity(Component):
	def __init__(self, dx, dy):
		self.dx = dx
		self.dy = dy
