from managers import *
from components import *
from systems import *
from resources import *
import pyglet

WIDTH = 800
HEIGHT = 650
pyg_window = pyglet.window.Window(width=WIDTH, height=HEIGHT)

entity_manager = EntityManager()
system_manager = SystemManager(entity_manager)

player = entity_manager.create_entity()
enemy = entity_manager.create_entity()

#Player setup#
entity_manager.add_component(player, cStringInfo('Player is in database'))
entity_manager.add_component(player, cPosition(WIDTH//2,HEIGHT//2))
entity_manager.add_component(player, cVelocity(5,5))
entity_manager.add_component(player, cTexture(i_player))

#Enemy setup#
entity_manager.add_component(enemy, cStringInfo('Enemy is in database'))
entity_manager.add_component(enemy, cPosition(WIDTH//3,HEIGHT//3))
entity_manager.add_component(enemy, cVelocity(2.5,2.5))
entity_manager.add_component(enemy, cTexture(i_enemy))

system_manager.add_system(sPrintInfo())
system_manager.add_system(sDraw())

### PYGLET STUFF ###
def update(dt):
	system_manager.update(dt)

@pyg_window.event
def on_draw():
	pyglet.gl.glClearColor(.5,.7,.2,1)
	pyg_window.clear()
	system_manager.find_system(type(sDraw())).update(None)

pyglet.clock.schedule(update)
pyglet.app.run()